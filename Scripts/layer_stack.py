# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 10:55:57 2019

@author: fenner.holman@kcl.ac.uk
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 11:37:56 2018

@author: fenner.holman@kcl.ac.uk
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 13:23:53 2018

@author: fenner.holman@kcl.ac.uk
"""


import numpy as np
import rasterio
from rasterio.mask import mask
import fiona


def normalise_dem(field,grnDEM,DEM):
    """Normalises crop Digital Elevation Model(DEM) using bare ground DEM.
    
    Checks alignment of the two DEMs before subtracting BG from crop DEM.
    
    Assigns WGS 1984 30N georeferencing to all layers.
    """
    dst_crs = ('epsg:32630')
    with rasterio.open(grnDEM,'r') as grnd:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            ground, out_transform=mask(grnd,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   grnd.crs, dst_crs, grnd.width, grnd.height, *grnd.bounds)
            ground=ground[0,...]
            dstny=np.empty([ground.shape[0],ground.shape[1]],dtype='float32')
             
    with rasterio.open(DEM,'r') as dem:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            crop, crop_transform=mask(dem,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   dem.crs, dst_crs, dem.width, dem.height, *dem.bounds)
            crops=crop[0,...]
            
            rasterio.warp.reproject(
                source=crops,
                destination=dstny,
                src_transform=crop_transform,
                src_crs=dem.crs,
                dst_transform=out_transform,
                dst_crs=dst_crs,
                resampling=rasterio.warp.Resampling.nearest)
    
    norm_dem=(dstny-ground)
    return(norm_dem)

def layerstacker(field,grnDEM,RGB,NIR,DEM,Thermal,dest):
    """Stacks multipe geotiff in to multi-layer single BigTiff file
    
    Requires shapefile defining area of intrerest to mask all geotiffs.
    
    Assigns WGS 1984 30N georeferencing to all layers.
    """
    dst_crs = ('epsg:32630')
    bands={}
    count=0
    if RGB=='blank':
        print('No RGB')
    else:
        count=count+3
        rgb_tiff=RGB
        with rasterio.open(rgb_tiff) as tiff:
            with fiona.open(field, "r") as src:
                features = [feature["geometry"] for feature in src]
                rgb, out_transform=mask(tiff,features,crop=True,nodata=0)
                transform, width, height = rasterio.warp.calculate_default_transform(
                   tiff.crs, dst_crs, tiff.width, tiff.height, *tiff.bounds)
                kwargs = tiff.meta.copy()
                
                r=rgb[0,:,:]
                
                bands['Blue']=rgb[2,:,:]
                bands['Green']=rgb[1,:,:]
                bands['Red']=rgb[0,:,:]
                
                kwargs.update({
                    'crs': dst_crs,
                    'transform': out_transform,
                    'width': r.shape[1],
                    'height': r.shape[0],
                    'count': 7
                })
                
                dstny=np.empty([r.shape[0],r.shape[1]],dtype='float32')
    
    if NIR=='blank':
        print('No NIR')
        
    else:
        count=count+1
        nir_tiff=NIR
        with rasterio.open(nir_tiff) as src1:
         with fiona.open(field, "r") as src:
            features = [feature["geometry"] for feature in src]
            nir, nir_transform=mask(src1,features,crop=True,nodata=0)
            nir=nir[0,:,:]   
    
            rasterio.warp.reproject(
                source=nir,
                destination=dstny,
                src_transform=nir_transform,
                src_crs=src1.crs,
                dst_transform=out_transform,
                dst_crs=dst_crs,
                resampling=rasterio.warp.Resampling.nearest)
            
            bands['NIR']=dstny
            if RGB=='blank':
                print('No NDVI')
            else:
                count=count+1
                ndvi=(dstny-r)/(dstny+r)
                #ndvi[np.isnan[ndvi]]=0
            bands['NDVI']=ndvi
    
    if DEM=='blank':
        print('No DEM')
    else:
        count=count+1
        demarray=normalise_dem(field,grnDEM,DEM)        
        bands['DEM']=demarray
    
    if Thermal=='blank':
        print('No Thermal')
    else:
        count=count+1
        Thermal_tiff=Thermal
        with rasterio.open(Thermal_tiff) as thermal1:
            with fiona.open(field, "r") as src:
                features = [feature["geometry"] for feature in src]
                thermal, thermal_transform=mask(thermal1,features,crop=True,nodata=0)
                
                thermalarray=np.empty([int(r.shape[0]/(thermal_transform[0]/out_transform[0])),int(r.shape[1]/(thermal_transform[0]/out_transform[0]))],dtype='float32')
                
                #dem_out_transform=out_transform
                thermal_outtransform=(thermal_transform[0], 0.0, out_transform[2],0.0, thermal_transform[4],out_transform[5])
        
                rasterio.warp.reproject(
                    source=thermal,
                    destination=thermalarray,
                    src_transform=thermal_transform,
                    src_crs=thermal1.crs,
                    dst_transform=thermal_outtransform,
                    dst_crs=dst_crs,
                    resampling=rasterio.warp.Resampling.nearest)
                
                bands['Thermal']=thermalarray
                kwargs.update({
                    'crs': dst_crs,
                    'transform': out_transform,
                    'width': r.shape[1],
                    'height': r.shape[0],
                    'count': count
                })
    print(list(bands.keys()),count)
    kwargs.update({'count':(len(bands))})
    with rasterio.open(dest,'w',**kwargs) as dst:
        
        for id, key in enumerate(bands.items()):
            dst.write_band(id+1,key[1])
            dst.set_band_description(id+1, key[0])
    print('Done!')
            
    
    
#    stacked=np.stack((b,g,r,dstny,ndvi))
#    dst.write(stacked)


