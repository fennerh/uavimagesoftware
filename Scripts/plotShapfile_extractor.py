# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 11:45:23 2018

@author: k1333986
"""
"""
Script takes any shapefile containing multiple separate shapes and produces inidividual shapefiles for each one.
"""

import fiona
import time
from osgeo import osr, ogr, gdal
import traceback

def get_shape_projection(inshape):
    
    #take input shapefile and get EPSG code to ensure correct projection of outputs#
    file = ogr.Open(inshape)
    layer = file.GetLayer()
    spatial_ref = int((layer.GetSpatialRef()).GetAttrValue('AUTHORITY',1))
    del file
    return spatial_ref

def write_shapefile(poly,pltid, out_shp, epsg):
    """
    https://gis.stackexchange.com/a/52708/8104
    """
    #define coordinate system
    data_projection = int(epsg)
    srs = osr.SpatialReference()
    tsrs=srs.ImportFromEPSG(data_projection)
    if tsrs != 0:
        raise RuntimeError(repr(tsrs) + ': could not import from EPSG')
    #print (srs.ExportToPrettyWkt())
    
    # Now convert it to a shapefile with OGR    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    ds = driver.CreateDataSource(out_shp)
    layer = ds.CreateLayer('',srs,ogr.wkbPolygon)
    # Add one attribute
    layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
    defn = layer.GetLayerDefn()

    ## If there are multiple geometries, put the "for" loop here

    # Create a new feature (attribute and geometry)
    feat = ogr.Feature(defn)
    feat.SetField('id', int(pltid))

    # Make a geometry, from Shapely object
    geom = ogr.CreateGeometryFromWkt(poly)
    feat.SetGeometry(geom)

    layer.CreateFeature(feat)
    feat = geom = None  # destroy these

    # Save and close everything
    ds = layer = feat = geom = None

def create_polygon(coords):          
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for coord in coords:
        ring.AddPoint(coord[0], coord[1])

    # Create polygon
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    return poly.ExportToWkt()

def main(coords,pltid,out_shp, epsg):
    poly = create_polygon(coords)
    write_shapefile(poly,pltid, out_shp, epsg)

def shapefile_gen(plots,out_folder):
    with fiona.open(plots) as shapes:
        for shape in shapes:
            #print ('ID: ',shape['properties']['Id'], 'Coordinates: ',shape['geometry']['coordinates'])
            coords=shape['geometry']['coordinates'][0]
            try:
                pltid=shape['properties']['PlotNumber']
            except:
                try:
                    pltid=shape['properties']['Plot_No']
                except:
                    pltid=shape['properties']['Id']
            X1=coords[0][0]
            X2=coords[1][0]
            X3=coords[2][0]
            X4=coords[3][0]
            Y1=coords[0][1]
            Y2=coords[1][1]
            Y3=coords[2][1]
            Y4=coords[3][1]
            
            plot = ogr.Geometry(ogr.wkbLinearRing)
            plot.AddPoint(X1, Y1)
            plot.AddPoint(X2, Y2)
            plot.AddPoint(X3, Y3)
            plot.AddPoint(X4, Y4)
            plot.AddPoint(X1, Y1)
            plotGeometry = ogr.Geometry(ogr.wkbPolygon)
            plotGeometry.AddGeometry(plot)
            minX, maxX, minY, maxY = plotGeometry.GetEnvelope()
            
            epsg = get_shape_projection(plots)
            
            main([(X1,Y1),(X2,Y2),(X3,Y3),(X4,Y4)],pltid, out_folder+'plot_'+str(pltid)+'.shp',epsg)
            #print(out_folder+'plot'+str(pltid)+'.shp')


