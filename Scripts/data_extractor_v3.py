# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 12:10:45 2019

@author: k1333986
"""
from osgeo import ogr, gdal, osr
import pandas as pd
import xlsxwriter
import os, re
import glob
import rasterio
from rasterio.mask import mask
from rasterio.warp import calculate_default_transform
import fiona
import numpy as np
from shapely.geometry import shape, mapping
import time
import cv2

def get_raster_projection(inraster):
    
    #take input raster/GeoTIFF and get EPSG code to ensure correct projection of outputs#
    file =  gdal.Open(inraster)
    layer = osr.SpatialReference(wkt=file.GetProjection())
    spatial_ref= int(layer.GetAttrValue('AUTHORITY',1))
    del file
    return spatial_ref

def normalise_dem(field,grnDEM,DEM):
    """Normalises crop Digital Elevation Model(DEM) using bare ground DEM.
    
    Checks alignment of the two DEMs before subtracting BG from crop DEM.
    
    Assigns WGS 1984 30N georeferencing to all layers.
    """
    dst_crs = ('epsg:'+str(get_raster_projection(DEM)))
    with rasterio.open(grnDEM,'r') as grnd:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            ground, out_transform=mask(grnd,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   grnd.crs, dst_crs, grnd.width, grnd.height, *grnd.bounds)
            ground=ground[0,...]
            dstny=np.empty([ground.shape[0],ground.shape[1]],dtype='float32')
             
    with rasterio.open(DEM,'r') as dem:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            crop, crop_transform=mask(dem,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   dem.crs, dst_crs, dem.width, dem.height, *dem.bounds)
            crops=crop[0,...]
            
            rasterio.warp.reproject(
                source=crops,
                destination=dstny,
                src_transform=crop_transform,
                src_crs=dem.crs,
                dst_transform=out_transform,
                dst_crs=dst_crs,
                resampling=rasterio.warp.Resampling.nearest)
    
    norm_dem=(dstny-ground)
    return(norm_dem)
    
def ndvi_calc(field,rgb,nir):
    """Calculates NDVI from red and near-infrared tiff layers."""
    dst_crs = ('EPSG:'+str(get_raster_projection(rgb)))
    with rasterio.open(rgb,'r') as grnd:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            ground, out_transform=mask(grnd,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   grnd.crs, dst_crs, grnd.width, grnd.height, *grnd.bounds)
            r=ground[0,...]
            dstny=np.empty([ground.shape[1],ground.shape[2]],dtype='float32')
            
    with rasterio.open(nir,'r') as dem:
        with fiona.open(field,'r') as src:
            features = [feature['geometry'] for feature in src]
            crop, crop_transform=mask(dem,features,crop=True,nodata=0)
            transform, width, height = rasterio.warp.calculate_default_transform(
                   dem.crs, dst_crs, dem.width, dem.height, *dem.bounds)
            crops=crop[0,...]
            
            rasterio.warp.reproject(
                source=crops,
                destination=dstny,
                src_transform=crop_transform,
                src_crs=dem.crs,
                dst_transform=out_transform,
                dst_crs=dst_crs,
                resampling=rasterio.warp.Resampling.nearest)
    
    ndvi=(dstny-r)/(dstny+r)
    return(ndvi)

def ExGR_mask(rgb_plot):
    """ Calucaltes Excess Green Red mask from red, green, blue geotiff layers."""
    rgb=np.float32(rgb_plot)
    #mx=np.ma.masked_where(rgb,np.isnan(rgb))
    try:
        rgb_float = cv2.normalize(rgb,None, 0.0, 1.0,cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        (r_norm, g_norm, b_norm) = cv2.split(rgb_float)
    except ValueError:
        rgb=np.moveaxis(rgb, 0, -1)
        rgb_float = cv2.normalize(rgb,None, 0.0, 1.0,cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        (r_norm, g_norm, b_norm) = cv2.split(rgb_float)
    ExG = 2 * g_norm - r_norm - b_norm
    ExR = 1.3 * r_norm - g_norm
    ExGR = ExG - ExR
    ExGR_reshaped = np.reshape(ExGR, np.size(ExGR, 0) * np.size(ExGR, 1) * 1)
    indx1 = np.where(ExGR_reshaped < 0)
    indx2 = np.where(ExGR_reshaped >= 0)
    ExGR_reshaped[indx1] = 0
    ExGR_reshaped[indx2] = 1
    ExGR = np.reshape(ExGR_reshaped, (np.size(ExGR, 0), np.size(ExGR, 1)))
    ExGR = ExGR.astype("uint8")    
    result = rgb_float.copy()
    result[ExGR==0] = (0,0,0)
    img_final = ExGR
    img_final[rgb[:,:,0]==0]=0
    return(img_final)
    

 
def data_extractor(variables,layers):
    """Extracts specific statistics for each layer using pre-defined shapefiles to define areas of interest.
    
    If R and NIR are present will also calculate NDVI; 
    If DEM and bare ground DEM present will calculate height;
    """
    orthos=[]
    print(layers)
    for layer in layers:
        #print(variables[variable])
        if layers[layer] != 'blank':
            orthos.append(layer)
        else:
            print ('no '+ layer)
    outfile= pd.ExcelWriter(variables['outfile'], engine='xlsxwriter') 
    print(orthos)
    ##Caluclate the maximum number of plots to interate over, based on filenaming of shapefiles##
    maxim=0
    for x in glob.glob(variables['shapefiles']+'*.shp'):
        y=int(re.split("[,.\/_]+",x)[-2])
        #print(y)
        if y>maxim:
            maxim=y
    
    for i in glob.glob(variables['shapefiles']+'*.shp'):
        indx=[x for x, elem in enumerate(re.split('(\d+)', os.path.normpath(i))) if 'shp' in elem]
        name=(re.split('(\d+)', os.path.normpath(i))[indx[0]-1])
        print (name)
        
        if 'RGB' in orthos:
            
            with rasterio.open(layers['RGB']) as tiff_rgb:
                with fiona.open(i, "r") as src:
                    features = [feature["geometry"] for feature in src]
                    rgb, out_transform=mask(tiff_rgb,features,crop=True,nodata=0)
                    
                    rgb = np.ma.masked_equal(rgb,0)
                    
                    r=rgb[0,...]
                    g=rgb[1,...]
                    b=rgb[2,...]
                    redcount=(rgb[0,...]>0).sum()
                    mskd=ExGR_mask(rgb[0:3,...])
                    
                    # r[r<=0]=np.nan
                    # g[g<=0]=np.nan
                    # b[b<=0]=np.nan
                    
                    bandname='Red'
                    cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
                    df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                    df.loc[int(name)-1].Plot=int(name)
                    df.loc[int(name)-1].Mean=np.ma.mean(r)
                    df.loc[int(name)-1].Median=np.ma.median(r)
                    df.loc[int(name)-1].StDv=np.ma.std(r)
                    df.loc[int(name)-1].Prcnt20=np.percentile(r.compressed(),20)
                    df.loc[int(name)-1].Prcnt80=np.percentile(r.compressed(),80)
                    df.loc[int(name)-1].CanopyPercent_ExGR=(((mskd==1).sum())/redcount)*100
                    print(((mskd==1).sum()),redcount)
                    df.to_excel(outfile,sheet_name=bandname)
                    
                    bandname='Green'
                    cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
                    df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                    df.loc[int(name)-1].Plot=int(name)
                    df.loc[int(name)-1].Mean=np.ma.mean(g)
                    df.loc[int(name)-1].Median=np.ma.median(g)
                    df.loc[int(name)-1].StDv=np.ma.std(g)
                    df.loc[int(name)-1].Prcnt20=np.percentile(g.compressed(),20)
                    df.loc[int(name)-1].Prcnt80=np.percentile(g.compressed(),80)
                    df.to_excel(outfile,sheet_name=bandname)
                    
                    bandname='Blue'
                    cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
                    df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                    df.loc[int(name)-1].Plot=int(name)
                    df.loc[int(name)-1].Mean=np.ma.mean(b)
                    df.loc[int(name)-1].Median=np.ma.median(b)
                    df.loc[int(name)-1].StDv=np.ma.std(b)
                    df.loc[int(name)-1].Prcnt20=np.percentile(b.compressed(),20)
                    df.loc[int(name)-1].Prcnt80=np.percentile(b.compressed(),80)
                    df.to_excel(outfile,sheet_name=bandname)
        
        if 'NIR' in orthos:
            
            with rasterio.open(layers['NIR']) as tiff_nir:
                with fiona.open(i, "r") as src:
                    features = [feature["geometry"] for feature in src]
                    nir, out_transform=mask(tiff_nir,features,crop=True,nodata=0)
                    
                    nir = np.ma.masked_equal(nir,0)

                    nir=nir[0,...]
                    # nir[nir<=0]=np.nan
                    
                    bandname='NIR'
                    cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
                    df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                    df.loc[int(name)-1].Plot=int(name)
                    df.loc[int(name)-1].Mean=np.ma.mean(nir)
                    df.loc[int(name)-1].Median=np.ma.median(nir)
                    df.loc[int(name)-1].StDv=np.ma.std(nir)
                    df.loc[int(name)-1].Prcnt20=np.percentile(nir.compressed(),20)
                    df.loc[int(name)-1].Prcnt80=np.percentile(nir.compressed(),80)
                    df.to_excel(outfile,sheet_name=bandname)

## Still need to change extractors below to work with mask arrays not nans###

        if 'NIR' in orthos:
            if 'RGB' in orthos:
                with rasterio.open(layers['RGB']) as tiff_rgb:
                    with rasterio.open(layers['NIR']) as tiff_nir:
                        with fiona.open(i, "r") as src:
                            features = [feature["geometry"] for feature in src]
                            rgb, out_transform=mask(tiff_rgb,features,crop=True,nodata=0)
                            nir, out_transform=mask(tiff_nir,features,crop=True,nodata=0)
                            
                            ndvi=ndvi_calc(i,layers['RGB'],layers['NIR'])             
                            redcount=(rgb[0,...]>0).sum()
                            mskd=ExGR_mask(rgb[0:3,...])                        
                            
                            inter=ndvi[~np.isnan(ndvi)]
                            
                            
                            inter70=inter[inter>np.nanpercentile(inter,70)]
                            inter90=inter[inter>np.nanpercentile(inter,90)]
                            bandname='NDVI'
                            cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt70','Prcnt80','Prcnt90','MEANp70','MEANp90','CanopyPercent_ExGR','CanopyNDVI_ExGR']
                            df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                            df.loc[int(name)-1].Plot=int(name)
                            df.loc[int(name)-1].Mean=np.nanmean(ndvi)
                            df.loc[int(name)-1].Median=np.nanmedian(ndvi)
                            df.loc[int(name)-1].StDv=np.nanstd(ndvi)
                            df.loc[int(name)-1].Prcnt20=np.nanpercentile(ndvi,20)
                            df.loc[int(name)-1].Prcnt70=np.nanpercentile(ndvi,70)
                            df.loc[int(name)-1].Prcnt80=np.nanpercentile(ndvi,80)
                            df.loc[int(name)-1].Prcnt90=np.nanpercentile(ndvi,90)
                            df.loc[int(name)-1].MEANp70=np.nanmean(inter70)
                            df.loc[int(name)-1].MEANp90=np.nanmean(inter90)
                            df.loc[int(name)-1].CanopyPercent_ExGR=(((mskd==1).sum())/redcount)*100
                            df.loc[int(name)-1].CanopyNDVI_ExGR=np.nanmean(ndvi[mskd==1])
                            df.to_excel(outfile,sheet_name=bandname)

        
        if 'DEM' in orthos:
         
            if 'groundDEM' in orthos:
                normDEM=normalise_dem(i,layers['groundDEM'],layers['DEM'])
                # normDEM[normDEM==0]=np.nan
                # normDEM[normDEM>100]=np.nan
                # normDEM[normDEM<-100]=np.nan
                
                normDEM = np.ma.masked_equal(normDEM,0)
                normDEM = np.ma.masked_greater(normDEM,100)
                normDEM = np.ma.masked_less(normDEM,-100)
                
                bandname='Crop Height'
                cols=['Plot','Mean','Median','StDv','Prcnt95','Prcnt99','CV','test']
                df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                df.loc[int(name)-1].Plot=int(name)
                df.loc[int(name)-1].Mean=np.ma.mean(normDEM)
                df.loc[int(name)-1].Median=np.ma.median(normDEM)
                df.loc[int(name)-1].StDv=np.ma.std(normDEM)
                df.loc[int(name)-1].Prcnt95=np.percentile(normDEM.compressed(),95)
                df.loc[int(name)-1].Prcnt99=np.percentile(normDEM.compressed(),99)
                df.loc[int(name)-1].test=np.ma.mean(normDEM[normDEM>=np.percentile(normDEM.compressed(),99)])
                df.loc[int(name)-1].CV=np.ma.std(normDEM)/np.ma.mean(normDEM)
                df.to_excel(outfile,sheet_name=bandname)
                
            else:
                dst_crs = ('epsg:'+str(get_raster_projection(layers['DEM'])))
                with rasterio.open(layers['DEM'],'r') as grnd:
                    with fiona.open(i, "r") as src:
                        features = [feature["geometry"] for feature in src]
                        ground, out_transform=mask(grnd,features,crop=True,nodata=0)
                        ground=ground[0,...]
                        
                        ground = np.ma.masked_equal(ground,0)
                
                        bandname='Crop Height'
                        cols=['Plot','Mean','Median','StDv','Prcnt95','Prcnt99','CV','test']
                        df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                        df.loc[int(name)-1].Plot=int(name)
                        df.loc[int(name)-1].Mean=np.ma.mean(ground)
                        df.loc[int(name)-1].Median=np.ma.median(ground)
                        df.loc[int(name)-1].StDv=np.ma.std(ground)
                        df.loc[int(name)-1].Prcnt95=np.percentile(ground.compressed(),95)
                        df.loc[int(name)-1].Prcnt99=np.percentile(ground.compressed(),99)
                        df.loc[int(name)-1].test=np.ma.mean(ground[ground>=np.percentile(ground.compressed(),99)])
                        df.loc[int(name)-1].CV=np.ma.std(ground)/np.ma.mean(ground)
                        df.to_excel(outfile,sheet_name=bandname)
        
        if 'datacube' in orthos:
            with rasterio.open(layers['datacube']) as Tiff:
                with fiona.open(i, "r") as src:
                    features = [feature["geometry"] for feature in src]
                    splot, out_transform=mask(Tiff,features,crop=True,nodata=0)
                    
            
                    for b in range(0,Tiff.count):
                        plt=splot[b,:,:]
                                
                        if Tiff.tags(b+1)['DESCRIPTION'] == 'NDVI':
                            inter=plt[~np.isnan(plt)]
                            inter70=inter[inter>np.nanpercentile(inter,70)]
                            inter90=inter[inter>np.nanpercentile(inter,90)]
                            rgb=np.dstack((splot[2,...],splot[1,...],splot[0,...]))
                            mskd=ExGR_mask(rgb)
                            redcount=(rgb[...,2]>0).sum()
                            
                            bandname=str(Tiff.tags(b+1)['DESCRIPTION'])
                            cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt70','Prcnt80','Prcnt90','MEANp70','MEANp90','CanopyPercent_ExGR','CanopyNDVI_ExGR']
                            df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))
                            df.loc[int(name)-1].Plot=int(name)
                            df.loc[int(name)-1].Mean=np.nanmean(plt)
                            df.loc[int(name)-1].Median=np.nanmedian(plt)
                            df.loc[int(name)-1].StDv=np.nanstd(plt)
                            df.loc[int(name)-1].Prcnt20=np.nanpercentile(plt,20)
                            df.loc[int(name)-1].Prcnt70=np.nanpercentile(plt,70)
                            df.loc[int(name)-1].Prcnt80=np.nanpercentile(plt,80)
                            df.loc[int(name)-1].Prcnt90=np.nanpercentile(plt,90)
                            df.loc[int(name)-1].MEANp70=np.nanmean(inter70)
                            df.loc[int(name)-1].MEANp90=np.nanmean(inter90)
                            df.loc[int(name)-1].CanopyPercent_ExGR=(((mskd==1).sum())/redcount)*100
                            df.loc[int(name)-1].CanopyNDVI_ExGR=np.nanmean(plt[mskd==1])
                            df.to_excel(outfile,sheet_name=bandname)
                            
                        elif Tiff.tags(b+1)['DESCRIPTION'] == 'DEM':
                            bandname=str(Tiff.tags(b+1)['DESCRIPTION'])
                            cols=['Plot','Mean','Median','StDv','Prcnt95','Prcnt99','test']
                            df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))                    
                            df.loc[int(name)-1].Plot=int(name)
                            df.loc[int(name)-1].Mean=np.nanmean(plt)
                            df.loc[int(name)-1].Median=np.nanmedian(plt)
                            df.loc[int(name)-1].StDv=np.nanstd(plt)
                            df.loc[int(name)-1].Prcnt95=np.nanpercentile(plt,95)
                            df.loc[int(name)-1].Prcnt99=np.nanpercentile(plt,99)
                            df.loc[int(name)-1].Prcnt99=np.nanmean(plt[plt>np.nanpercentile(plt,99)])
                            df.to_excel(outfile,sheet_name=bandname) 
                            
                        else:
                            bandname=str(Tiff.tags(b+1)['DESCRIPTION'])
                            cols=['Plot','Mean','Median','StDv','Prcnt20','Prcnt80']
                            df=pd.DataFrame(columns=cols,index=range(0,(maxim+1)))                     
                            df.loc[int(name)-1].Plot=int(name)
                            df.loc[int(name)-1].Mean=np.nanmean(plt)
                            df.loc[int(name)-1].Median=np.nanmedian(plt)
                            df.loc[int(name)-1].StDv=np.nanstd(plt)
                            df.loc[int(name)-1].Prcnt20=np.nanpercentile(plt,20)
                            df.loc[int(name)-1].Prcnt80=np.nanpercentile(plt,80)
                            df.to_excel(outfile,sheet_name=bandname) 
                   
    outfile.save()