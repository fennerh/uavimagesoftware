# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 14:44:55 2019

@author: Fenner
"""
def exif_corrector(infolder,outfolder):
    """Uses Exiftool.exe. to copy exif data from original images to newly calibrated reflectance images."""
    for image in find_file(outfolder,'tiff'):
            #print('\tConverting '+image)
            name=(image.replace('.','/'))
            indx=[i for i, elem in enumerate(name.split('/')) if 'DSC' in elem]
            raw_img=infolder+name.split('/')[indx[0]]+'.ARW'
            exiftool='C:/Users/Fenner/Google Drive/Conversion_software_V2/exiftool.exe'
            exiftool_params=['%s'%exiftool,'-TagsFromFile','%s'%raw_img,'--Orientation','-author=FHolman','-overwrite_original','%s'%image]
            subprocess.run(exiftool_params)
            #print('\tConverted '+image)