# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
import glob, os
rasterio_imports_paths = glob.glob(r'C:\Users\k1333986\AppData\Local\Continuum\anaconda3\envs\Software\Lib\site-packages\rasterio\*.py')
fiona_imports_paths = glob.glob(r'C:\Users\k1333986\AppData\Local\Continuum\anaconda3\envs\Software\Lib\site-packages\fiona\*.py')
extra_imports = ['rasterio._shim','fiona._shim','fiona.schema','tifffile._tifffile','xlsxwriter','rasterio.control']

for item in rasterio_imports_paths:
    current_module_filename = os.path.split(item)[-1]
    current_module_filename = 'rasterio.'+current_module_filename.replace('.py', '')
    extra_imports.append(current_module_filename)

for item in fiona_imports_paths:
    current_module_filename = os.path.split(item)[-1]
    current_module_filename = 'fiona.'+current_module_filename.replace('.py', '')
    extra_imports.append(current_module_filename)

a = Analysis(['GUI.py'],
             pathex=['C:\\Users\\k1333986\\Google Drive\\Camera_software_V3\\Scripts'],
             binaries=[],
             datas=[('C:\\Users\\k1333986\\Google Drive\\Camera_software_V3\\Scripts\\*.txt','.'),('C:\\Users\\k1333986\\Google Drive\\Camera_software_V3\\*.exe','.'),
		('C:\\Users\\k1333986\\Google Drive\\Camera_software_V3\\Dark_images\\','Dark_images')],
             hiddenimports=extra_imports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='UAVImageConverter',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
	  icon='C:\\Users\k1333986\\Google Drive\\Camera_software_V3\\Scripts\\ICON.ico' )
