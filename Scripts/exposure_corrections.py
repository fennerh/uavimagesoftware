# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 10:47:56 2018

@author: fenner.holman@kcl.ac.uk

Script to read exif data of camera images (not sure whether it is file specific - need to check the requirements of exifread) and generate correction factors for:
    1. Normalise aperture values to the equivalent of F/1
    2. Normalise shutter speed to the equivalent of 1/500
    3. Normalise ISO values to the equivalent of ISO 100.

"""
import numpy as np
import exifread

def aperture_correction(image):
    """Calucaltes aperture correction factor.
    
    Correction factor = 1/(fnumber)^2
    """
    tags=exifread.process_file(open(image,'rb'),details=False)
    while True:
        fn=(str(tags['EXIF FNumber']))
        try:
            top, bottom=fn.split('/')
            fnumber=(int(top)/int(bottom))
            fnumber=np.round(fnumber,2)
            break
        except ValueError:
            fnumber=int(fn)
            break
    correction=1/(fnumber**2) 
    return(correction)
    
def iso_correction(image):
    """Calucaltes ISO correction factor.
    
    Correction factor = ISO/100
    """
    tags=exifread.process_file(open(image,'rb'),details=False)
    iso=(str(tags['EXIF ISOSpeedRatings']))
    isoc=int(iso)/100
    return(isoc)

def shutter_correction(image):
    """Calucaltes shutter speed correction factor.
    
    Correction factor = Shutter Spped/500
    """
    tags=exifread.process_file(open(image,'rb'),details=False)
    while True:
        shutter=(str(tags['EXIF ExposureTime']))
        try:
            top, bottom=shutter.split('/')
            shutC=((int(bottom)/int(top))/500)
            break
        except ValueError:
            shutC=int(shutter)/500
            break
    return(shutC)