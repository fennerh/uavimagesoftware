# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 10:24:15 2018

@author: fenner.holman@kcl.ac.uk
"""
import tifffile as tiff
import numpy as np
import pandas as pd
import os, re
import glob
import exifread
from datetime import datetime
import math
import time
from irradiance_corrections import irradiance_correction
from vignetting_modeller import vignetting_corretion
from exposure_corrections import *

def correct_images(infolder,outfolder,vigfolder,t5file,camera,average):
    """ Corrects images for camera settings, vignetting and calibrates for irradiance. 
    Equations in step 6 of correction steps are custom and camera specific.
    """
    for im in find_file(infolder,'tiff'):
        if camera =='NIR':            
            image=tiff.imread(im)[:,:,2]            
            name=(im.replace('.','/'))
            indx=[i for i, elem in enumerate(name.split('/')) if 'DSC' in elem]
            name=name.split('/')[indx[0]]
            
            c1= image/iso_correction(im)
            c2=c1/aperture_correction(im)
            c3=c2*shutter_correction(im)
            c4=c3*vignetting_corretion(im,vigfolder,camera)   
            c5=c4/irradiance_correction(im,t5file,camera,average) 
            c6=0.0214*c5-0.0053
            c7=(c6).astype(np.float16)
            tiff.imsave(im,c7)
        if camera =='RGB':            
            r=tiff.imread(im)[:,:,0]
            g=tiff.imread(im)[:,:,1]
            b=tiff.imread(im)[:,:,2]
            name=(im.replace('.','/'))
            indx=[i for i, elem in enumerate(name.split('/')) if 'DSC' in elem]
            name=name.split('/')[indx[0]]
            r1=r/iso_correction(im)
            r2=r1/aperture_correction(im)
            r3=r2*shutter_correction(im)
            r4=r3*(vignetting_corretion(im,vigfolder,camera)[:,:,0])
            r5=r4/irradiance_correction(im,t5file,'R',average) 
            r6=0.0189*r5-0.00169
            
            g1=g/iso_correction(im)
            g2=g1/aperture_correction(im)
            g3=g2*shutter_correction(im)
            g4=g3*(vignetting_corretion(im,vigfolder,camera)[:,:,1])
            g5=g4/irradiance_correction(im,t5file,'G',average) 
            g6=0.00746*g5-0.00478
            
            b1=b/iso_correction(im)
            b2=b1/aperture_correction(im)
            b3=b2*shutter_correction(im)
            b4=b3*(vignetting_corretion(im,vigfolder,camera)[:,:,2])
            b5=b4/irradiance_correction(im,t5file,'B',average) 
            b6=0.01*b5-0.00285
            
            c6=np.dstack([r6,g6,b6])
            c7=(c6).astype(np.float16)
            tiff.imsave(im,c7)
        
def find_file(path, extention):
    """Find files matching specified file extention within folder."""
    os.chdir(path)
    results=[]
    for file in glob.glob('*.'+extention):
        results.append(os.path.join(path,file))
    return (results)
def move_images(source,destination):
    """Move all TIFF images from source to destination folder."""
    if os.path.exists(destination):
        None
    else:
        os.mkdir(destination)
    for im in find_file(source,'tiff'):
        os.rename(os.path.join(source,im),os.path.join(destination,os.path.split(im)[1]))
    return