# -*- coding: utf-8 -*-
"""
Created on Sat Dec 15 12:17:07 2018

@author: fenner.holman@kcl.ac.uk
"""
## Defines current working directory even in executable.
def resource_path(relative_path):
    """ Get absolute path to resource, works for development and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)
import os,sys


#print(os.environ["GDAL_DATA"])
# print(sys._MEIPASS)
os.environ["GDAL_DATA"] = resource_path('gdal')
#os.environ["GEOTIFF_CSV"] = resource_path('\Library\share\epsg_csv')
os.environ["PROJ_LIB"] = resource_path('proj')
os.environ["USE_PATH_FOR_GDAL_PYTHON"] = 'YES'

print(os.environ["GDAL_DATA"])
#os.environ['PROJ_LIB'] = r'C:\Users\fenner\Anaconda3\envs\sftwr\Library\share\proj'
import math
import time
import shutil
import exifread
import threading
import subprocess
import numpy as np
import glob, re
import pandas as pd
import tkinter as tk
import tifffile as tiff
from datetime import datetime
from osgeo import ogr, gdal
import rasterio
from rasterio.mask import mask
import fiona
from shapely.geometry import shape, mapping
import cv2
import traceback
import xlsxwriter

abspath = os.path.abspath(__file__)
dname = (os.path.dirname(abspath))
os.chdir(dname)
print(dname)

from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import font
from tkinter.ttk import Style
from queue import Queue
from vignetting_modeller import *
from image_corrector import *
# from data_extractor_v3 import *
# from layer_stack import *
from exposure_corrections import *
from irradiance_corrections import *
from plotShapfile_extractor import *



## Define style variables for Tkinter##
Large_Font=("Verdana",12,'bold')
Norm_Font=("Verdana",10)
Small_font=("Verdana",8)

## Define text files for saving locations of shapefiles##
fieldfile=open(resource_path('fieldshapefile.txt'),'r+')
fieldshape=fieldfile.read()
file=open(resource_path('plotshapefiles.txt'),'r+')
shapefolder=file.read()
groundfile=open(resource_path('groundDEM.txt'),'r+')
grnddem=groundfile.read()

## Find File Tool: - it searches and lists all files within a folder based on provided file extention
def find_file(path, extention):
    """Find files matching specified file extention within folder."""
    os.chdir(path)
    results=[]
    for file in glob.glob('*.'+extention):
        results.append(os.path.join(path,file))
    return (results)

## Move Images Tool: - Move images from one location to another.
def move_images(source,destination):
    """Move all TIFF images from source to destination folder."""
    if os.path.exists(destination):
        None
    else:
        os.mkdir(destination)
    for im in find_file(source,'tiff'):
        shutil.move(os.path.join(source,im),os.path.join(destination,os.path.split(im)[1]))
    return

## Exif Corrector Tool: - Uses the CMD line ExifTool to copy exif data from original images to newly calibrated Tiff images.
def exif_corrector(infolder,outfolder):
    """Uses Exiftool.exe. to copy exif data from original images to newly calibrated reflectance images."""
    for image in find_file(outfolder,'tiff'):
            raw_img= os.path.join(infolder,(re.split('([\\\/])',image)[-1].split('.')[0]+'.ARW'))
            exiftool=resource_path('exiftool.exe')
            exiftool_params=['%s'%exiftool,'-TagsFromFile','%s'%raw_img,'--Orientation','-author=FHolman','-overwrite_original','%s'%image]
            subprocess.run(exiftool_params)

## Task: - used to run background processes.
def task(root2):
    ft = ttk.Frame(root2)
    ft.grid(column=2,row=9,padx=10,pady=10)
    pb_hD = ttk.Progressbar(ft, orient='horizontal', mode='indeterminate')
    pb_hD.grid()
    pb_hD.start(50)
    root2.mainloop()

## Process Images Tool: - The tool that performs the calibration steps (conversion, vignetting modelling, and  for convverting raw imagery to reflectance.
def process_images(root2,variables):
    infolder=variables['infolder']
    outfolder=variables['outfolder']
    vigdest=variables['vigdest']
    camera=variables['camera']
    t5file=variables['t5file']
    average=variables['average']
    try:
        print('Converting Raw Imagery')
        for image in find_file(infolder,'ARW'):
            #print('\tConverting '+image)
            darkim=resource_path('Dark_images/'+camera+'_Dark.pgm')
            dcraw=resource_path('dcraw.exe')
            dcraw_params=['%s'%dcraw,'-6','-W','-g','1','1','-T','-q','0','-o','0','-r','1','1','1','1','-t','0','-K','%s'%darkim,'%s'%image ]
            subprocess.run(dcraw_params)
            #print('\tConverted '+image)
            move_images(infolder,outfolder)
    except Exception as e:
        tk.messagebox.showerror("Error", e)
        traceback.print_exc()
        root2.quit()
        
    print ('Raw conversion complete')
    try:
        print ('Generating vignetting filters')
        vigmodeller(outfolder,vigdest,camera)
    except Exception as e:
        tk.messagebox.showerror("Error", e)
        traceback.print_exc()
        root2.quit()
    print ('Vignetting Complete')
    try:
        print('Correcting Images')
        correct_images(outfolder,outfolder,vigdest,t5file,camera,average)
        exif_corrector(infolder,outfolder)
    except Exception as e:
        tk.messagebox.showerror("Error", e)
        traceback.print_exc()
        root2.quit()
    print('Done')
    root2.quit()

## Software Class: Defines the Tkinter variables for the GUI software.
class software(tk.Tk):
    def __init__(self,*args,**kwargs):
        tk.Tk.__init__(self,*args,**kwargs)

        tk.Tk.wm_title(self,"UAV Data Tool")

        container=tk.Frame(self)
        container.pack(side='top',fill='both',expand=True)
        container.grid_rowconfigure(0,weight=1)
        container.grid_columnconfigure(0,weight=1)

        self.frames={}
        for F in (HomePage,ImageCalibrator,batchcalibrator,Shapefilegenerator): #DataMerger,DataExtractor
            frame=F(container,self)
            self.frames[F]=frame
            frame.grid(row=0,column=0,sticky='nsew')
        self.show_frame(HomePage)

    def show_frame(self,cont):
        frame=self.frames[cont]
        frame.tkraise()
    def enditall(self):
        global file
        file.close()
        self.destroy()

## Homepage Class: Defines the variables for the appearance and function of the software homepage.
class HomePage(ttk.Frame):
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)

        label=tk.Label(self,text='Home Page',font=Large_Font)
        label.pack(pady=10,padx=10)

        button1=ttk.Button(self,text='Image Calibration Tool',
                          command=lambda: controller.show_frame(ImageCalibrator))
        button1.pack(pady=10)

        # button2=ttk.Button(self,text='Data Merging Tool',
        #                   command=lambda: controller.show_frame(DataMerger))
        # button2.pack(pady=10)

        # button3=ttk.Button(self,text='Data Extraction Tool',
        #                   command=lambda: controller.show_frame(DataExtractor))
        # button3.pack(pady=10)

        button4=ttk.Button(self,text='Shapefile Generator',
                          command=lambda: controller.show_frame(Shapefilegenerator))
        button4.pack(pady=10)
       
        button5=ttk.Button(self,text='Quit App',
                          command=controller.enditall)
        button5.pack(pady=10)


## Image Calibrator Class: defines the variables for the appearance and function of the Image Calibration Tool.
class ImageCalibrator(ttk.Frame):

    def get_raw(self):
        self.button5.configure(state='enabled')
        folder=tk.filedialog.askdirectory(initialdir = "/",title = 'Select Raw Folder')
        self.rawfolder.set(folder+'/')
        try:
            self.t5file.set((glob.glob(os.path.abspath(os.path.join(self.rawfolder.get(),'../'))+'/'+'*ec5*'+'*.xlsx'))[0])
        except:
            print('No Tec5 file found')
            self.t5file.set('blank')
        self.vigfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+'VIG_models\\'),''))
        try:
            self.cam.set(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('NIR')])
            self.outfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('NIR')])+'_AllCorrect'),''))
        except:
            self.cam.set(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('RGB')])
            self.outfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('RGB')])+'_AllCorrect'),''))

    def get_t5(self):
        try:
            folder=tk.filedialog.askopenfilename(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Tec5 File',filetypes = (("excel files","*.xlsx"),("all files","*.*")))
            self.t5file.set(folder)
        except:
            print('No Tec5 file found')
            self.t5file.set('blank')
        
    def get_vig(self):
        folder=tk.filedialog.askdirectory(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Vignette Model Folder')
        self.vigfolder.set(folder+'/')

    def get_out(self):
        folder=tk.filedialog.askdirectory(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Output Folder')
        self.outfolder.set(folder+'/')

    def add2batch(self):
        self.button5.configure(state='enabled')
        batch_vars={'infolder':self.rawfolder.get(),'outfolder':self.outfolder.get(),'t5file':self.t5file.get(),'vigdest':self.vigfolder.get(),'camera':self.cam.get()}
        self.batch.append(batch_vars)
        self.batchbox.insert('end',str(batch_vars))
        self.button9.configure(state='enabled')

    def deletebatch(self):
        self.batch.pop(-1)
        self.batchbox.delete('1.0','end')
        self.batchbox.insert('end',self.batch)


    def _toggle_state(self, state):
        state = state if state in ('normal', 'disabled') else 'normal'
        widgets = (self.button1, self.button2,self.button3,self.button4,self.button5,self.rgb,self.button8)
        for widget in widgets:
            widget.configure(state=state)

    def click_run(self):
        self._toggle_state('disabled')
        tk.messagebox.showinfo("Work Started", "Processing Started")
        variables={'infolder':self.rawfolder.get(),'outfolder':self.outfolder.get(),'t5file':self.t5file.get(),'vigdest':self.vigfolder.get(),'camera':self.cam.get(),'average':self.average.get()}
        root2=tk.Toplevel()
        t1=threading.Thread(target=process_images, args=(root2,variables,))
        t1.start()
        task(root2)  # This will block while the mainloop runs
        t1.join()
        root2.destroy()
        tk.messagebox.showinfo("Processing Complete", "Processing Complete")
        self._toggle_state('normal')

    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        self.grid_rowconfigure(12, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(4, weight=1)

#        label=tk.Label(self,text='Image Calibration',font=Large_Font)
#        label.pack(pady=10,padx=10)

#        button1=ttk.Button(self,text='Back to Home Page',
#                          command=lambda: controller.show_frame(HomePage))
#        button1.pack()
        #---VARIABLES---#
        self.rawfolder=tk.StringVar()
        self.t5file=tk.StringVar()
        self.vigfolder=tk.StringVar()
        self.outfolder=tk.StringVar()
        self.cam=tk.StringVar()
        self.queue=Queue()
        options=['Please Select*','NIR','RGB']
        self.batch=[]
        self.average=tk.IntVar()


        #---LABELS---#
        self.label=tk.Label(self,text='Image Calibration',font=Large_Font)
        self.label.columnconfigure(0,weight=1)
        self.label.grid(row=0,column=2,padx=10)

        self.label1=tk.Label(self,text='Folder containing Raw (.ARW) image files.')
        self.label1.columnconfigure(0,weight=1)
        self.label1.grid(row=1,column=4,padx=10)

        self.label2=tk.Label(self,text='Tec5 file of irradiance data.')
        self.label2.columnconfigure(0,weight=1)
        self.label2.grid(row=2,column=4,padx=10)

        self.label3=tk.Label(self,text='Folder containing vignetting models.')
        self.label3.columnconfigure(0,weight=1)
        self.label3.grid(row=3,column=4,padx=10)

        self.label4=tk.Label(self,text='Folder to save processed images.')
        self.label4.columnconfigure(0,weight=1)
        self.label4.grid(row=4,column=4,padx=10)

        self.label5=tk.Label(self,text='Select which camera is being processed.')
        self.label5.columnconfigure(0,weight=1)
        self.label5.grid(row=5,column=4,padx=10)

        #---BUTTONS---#
        self.button1=ttk.Button(self,text='Raw Imagery',command=self.get_raw,width=20)
        self.button1.columnconfigure(0,weight=1)
        self.button1.grid(row=1,column=1,pady=10)

        self.button2=ttk.Button(self,text='Tec5 File',command=self.get_t5,width=20)
        self.button2.columnconfigure(0,weight=1)
        self.button2.grid(row=2,column=1,pady=10)

        self.button3=ttk.Button(self,text='Vignetting Models',command=self.get_vig,width=20)
        self.button3.columnconfigure(0,weight=1)
        self.button3.grid(row=3,column=1,pady=10)

        self.button4=ttk.Button(self,text='Out Folder',command=self.get_out,width=20)
        self.button3.columnconfigure(0,weight=1)
        self.button4.grid(row=4,column=1,pady=10)

        self.rgb=ttk.OptionMenu(self,self.cam,*options)
        self.rgb.columnconfigure(0,weight=1)
        self.rgb.grid(row=5,column=2,pady=10)

        self.button5=ttk.Button(self,text='Run',command=self.click_run)
        self.button5.columnconfigure(0,weight=1)
        self.button5.configure(state='disabled')
        self.button5.grid(row=6,column=2)
#
        self.button6=ttk.Button(self,text='Batch Process',command=lambda: controller.show_frame(batchcalibrator))
        self.button6.columnconfigure(0,weight=1)
        self.button6.configure(state='enabled')
        self.button6.grid(row=12,column=2)
#
        self.button7=ttk.Checkbutton(self,text='Average Irradiance',variable=self.average)
        self.button7.columnconfigure(0,weight=1)
        self.button7.grid(row=2,column=3)
#
#        self.button17=ttk.Button(self,text='Delete',command=self.deletebatch)
#        self.button17.columnconfigure(0,weight=1)
#        self.button17.grid(row=4,column=2)

#        self.button9=ttk.Button(self,text='Batch_Run',command=self.batchrun)
#        self.button9.columnconfigure(0,weight=1)
#        self.button9.configure(state='disabled')
#        self.button9.grid(row=6,column=4)

        self.button8=ttk.Button(self,text='Back to Home Page',command=lambda: controller.show_frame(HomePage))
        self.button8.columnconfigure(0,weight=1)
        self.button8.grid(row=8,column=2,pady=5)

#        self.button9=ttk.Button(self,text='test',command=lambda: popup('this is a test'))
#        self.button9.grid(row=7,column=2)

        self.button10=ttk.Button(self,text='Quit',command=controller.enditall)
        self.button10.columnconfigure(0,weight=1)
        self.button10.grid(row=9,column=2,pady=5)

        #---ENTRIES---#
        self.entry1=ttk.Entry(self,textvariable=self.rawfolder,width=50)
        self.entry1.columnconfigure(0,weight=1)
        self.entry1.grid(row=1,column=2,padx=5)
        self.entry2=ttk.Entry(self,textvariable=self.t5file,width=50)
        self.entry2.columnconfigure(0,weight=1)
        self.entry2.grid(row=2,column=2,padx=5)
        self.entry3=ttk.Entry(self,textvariable=self.vigfolder,width=50)
        self.entry3.columnconfigure(0,weight=1)
        self.entry3.grid(row=3,column=2,padx=5)
        self.entry4=ttk.Entry(self,textvariable=self.outfolder,width=50)
        self.entry4.columnconfigure(0,weight=1)
        self.entry4.grid(row=4,column=2,padx=5)
#        self.batchbox=tk.Text(self,width=50,height=10)
#        self.batchbox.columnconfigure(0,weight=1)
#        self.batchbox.grid(row=6,column=2,columnspan=2)

## Image Calibrator Class: defines the variables for the appearance and function of the Batch Processing Image Calibration Tool.
class batchcalibrator(ttk.Frame):

    def get_raw(self):
        self.button5.configure(state='enabled')
        folder=tk.filedialog.askdirectory(initialdir = "/",title = 'Select Raw Folder')
        self.rawfolder.set(folder+'/')
        try:
            self.t5file.set((glob.glob(os.path.abspath(os.path.join(self.rawfolder.get(),'../'))+'/'+'*ec5*'+'*.xlsx'))[0])
        except:
            tk.messagebox.showinfo('Error','No Tec5 file found')
            self.t5file.set('blank')
        self.vigfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+'VIG_models\\'),''))
        try:
            self.cam.set(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('NIR')])
            self.outfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('NIR')])+'_AllCorrect'),''))
        except:
            self.cam.set(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('RGB')])
            self.outfolder.set(os.path.join(os.path.abspath(os.path.join(self.rawfolder.get(),"../")+(re.split('/',self.rawfolder.get())[re.split('/',self.rawfolder.get()).index('RGB')])+'_AllCorrect'),''))

    def get_t5(self):
        try:
            folder=tk.filedialog.askopenfilename(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Tec5 File',filetypes = (("excel files","*.xlsx"),("all files","*.*")))
            self.t5file.set(folder)
        except:
            tk.messagebox.showinfo('Error','No Tec5 file found')
            self.t5file.set('blank')

    def get_vig(self):
        folder=tk.filedialog.askdirectory(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Vignette Model Folder')
        self.vigfolder.set(folder+'/')

    def get_out(self):
        folder=tk.filedialog.askdirectory(initialdir = os.path.abspath(os.path.join(self.rawfolder.get() ,"../")),title = 'Select Output Folder')
        self.outfolder.set(folder+'/')

    def add2batch(self):
        batch_vars={'infolder':self.rawfolder.get(),'outfolder':self.outfolder.get(),'t5file':self.t5file.get(),'vigdest':self.vigfolder.get(),'camera':self.cam.get(),'average':self.average.get()}
        self.batch.append(batch_vars)
        self.batchbox.insert('end',str(batch_vars))
        self.button9.configure(state='enabled')

    def deletebatch(self):
        self.batch.pop(-1)
        self.batchbox.delete('1.0','end')
        self.batchbox.insert('end',self.batch)


    def _toggle_state(self, state):
        state = state if state in ('normal', 'disabled') else 'normal'
        widgets = (self.button1, self.button2,self.button3,self.button4,self.button5,self.rgb,self.button8)
        for widget in widgets:
            widget.configure(state=state)

    def batchrun(self):
        self._toggle_state('disabled')
        tk.messagebox.showinfo("Work Started", "Processing Started")
        root2=tk.Toplevel()
        for batch in self.batch:
            batch['root2']='root2'
            print(batch)
            t1=threading.Thread(target=process_images, args=(root2,batch,))
            t1.start()
            task(root2)  # This will block while the mainloop runs
            t1.join()
        root2.destroy()
        tk.messagebox.showinfo("Processing Complete", "Processing Complete")
        self._toggle_state('normal')

    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        self.grid_rowconfigure(12, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(4, weight=1)

#        label=tk.Label(self,text='Image Calibration',font=Large_Font)
#        label.pack(pady=10,padx=10)

#        button1=ttk.Button(self,text='Back to Home Page',
#                          command=lambda: controller.show_frame(HomePage))
#        button1.pack()
        #---VARIABLES---#
        self.rawfolder=tk.StringVar()
        self.t5file=tk.StringVar()
        self.vigfolder=tk.StringVar()
        self.outfolder=tk.StringVar()
        self.cam=tk.StringVar()
        self.queue=Queue()
        options=['Please Select*','NIR','RGB']
        self.batch=[]
        self.average=tk.IntVar()


        #---LABELS---#
        self.label=tk.Label(self,text='Image Calibration',font=Large_Font)
        self.label.columnconfigure(0,weight=1)
        self.label.grid(row=0,column=2,padx=10)

        self.label1=tk.Label(self,text='Folder containing Raw (.ARW) image files.')
        self.label1.columnconfigure(0,weight=1)
        self.label1.grid(row=1,column=4,padx=10)

        self.label2=tk.Label(self,text='Tec5 file of irradiance data.')
        self.label2.columnconfigure(0,weight=1)
        self.label2.grid(row=2,column=4,padx=10)

        self.label3=tk.Label(self,text='Folder containing vignetting models.')
        self.label3.columnconfigure(0,weight=1)
        self.label3.grid(row=3,column=4,padx=10)

        self.label4=tk.Label(self,text='Folder to save processed images.')
        self.label4.columnconfigure(0,weight=1)
        self.label4.grid(row=4,column=4,padx=10)

        self.label5=tk.Label(self,text='Select which camera is being processed.')
        self.label5.columnconfigure(0,weight=1)
        self.label5.grid(row=5,column=4,padx=10)

        #---BUTTONS---#
        self.button1=ttk.Button(self,text='Raw Imagery',command=self.get_raw,width=20)
        self.button1.columnconfigure(0,weight=1)
        self.button1.grid(row=1,column=1,pady=10)

        self.button2=ttk.Button(self,text='Tec5 File',command=self.get_t5,width=20)
        self.button2.columnconfigure(0,weight=1)
        self.button2.grid(row=2,column=1,pady=10)

        self.button3=ttk.Button(self,text='Vignetting Models',command=self.get_vig,width=20)
        self.button3.columnconfigure(0,weight=1)
        self.button3.grid(row=3,column=1,pady=10)

        self.button4=ttk.Button(self,text='Out Folder',command=self.get_out,width=20)
        self.button3.columnconfigure(0,weight=1)
        self.button4.grid(row=4,column=1,pady=10)

        self.rgb=ttk.OptionMenu(self,self.cam,*options)
        self.rgb.columnconfigure(0,weight=1)
        self.rgb.grid(row=5,column=2,pady=10)

        self.button5=ttk.Button(self,text='Batch Run',command=self.batchrun)
        self.button5.columnconfigure(0,weight=1)
        self.button5.configure(state='disabled')
        self.button5.grid(row=9,column=2)

        self.button7=ttk.Button(self,text='Add',command=self.add2batch)
        self.button7.columnconfigure(0,weight=1)
        self.button7.grid(row=6,column=2)

        self.button17=ttk.Button(self,text='Delete',command=self.deletebatch)
        self.button17.columnconfigure(0,weight=1)
        self.button17.grid(row=7,column=2)
        
        self.button27=ttk.Checkbutton(self,text='Average Irradiance',variable=self.average)
        self.button27.columnconfigure(0,weight=1)
        self.button27.grid(row=2,column=3)

        self.button8=ttk.Button(self,text='Back to Home Page',command=lambda: controller.show_frame(HomePage))
        self.button8.columnconfigure(0,weight=1)
        self.button8.grid(row=11,column=2,pady=5)

        self.button9=ttk.Button(self,text='Back',command=lambda: controller.show_frame(ImageCalibrator))
        self.button9.columnconfigure(0,weight=1)
        self.button9.grid(row=10,column=2,pady=5)

        self.button10=ttk.Button(self,text='Quit',command=controller.enditall)
        self.button10.columnconfigure(0,weight=1)
        self.button10.grid(row=12,column=2,pady=5)

        #---ENTRIES---#
        self.entry1=ttk.Entry(self,textvariable=self.rawfolder,width=50)
        self.entry1.columnconfigure(0,weight=1)
        self.entry1.grid(row=1,column=2,padx=5)
        self.entry2=ttk.Entry(self,textvariable=self.t5file,width=50)
        self.entry2.columnconfigure(0,weight=1)
        self.entry2.grid(row=2,column=2,padx=5)
        self.entry3=ttk.Entry(self,textvariable=self.vigfolder,width=50)
        self.entry3.columnconfigure(0,weight=1)
        self.entry3.grid(row=3,column=2,padx=5)
        self.entry4=ttk.Entry(self,textvariable=self.outfolder,width=50)
        self.entry4.columnconfigure(0,weight=1)
        self.entry4.grid(row=4,column=2,padx=5)
        self.batchbox=tk.Text(self,width=50,height=10)
        self.batchbox.columnconfigure(0,weight=1)
        self.batchbox.grid(row=8,column=1,columnspan=3, padx=10, pady=5)
#        canvas=FigureCanvasTkAgg(f,self)
#        canvas.draw()
#        canvas.get_tk_widget().pack(side=tk.BOTTOM,fill=tk.BOTH,expand=True)

## Image Calibrator Class: defines the variables for the appearance and function of the Data Merging Tool.
# class DataMerger(ttk.Frame):

#     def get_shapefile(self):
#         global fieldfile
#         global fieldshape
#         folder=tk.filedialog.askopenfilename(initialdir = "/",title = 'Shapefiles',filetypes=(("shp","*.shp"),("all files","*.*")))
#         if folder==fieldshape:
#             pass
#         else:
#             self.shapefiles.set(folder)
#             fieldfile.seek(0)
#             fieldfile.write(folder)
#             fieldfile.truncate()
#     def get_grndDEM(self):
#         global groundfile
#         global grnddem
#         folder=tk.filedialog.askopenfilename(initialdir = "/",title = 'Bare Ground DEM',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         if folder==grnddem:
#             pass
#         else:
#             self.groundDEM.set(folder)
#             file.seek(0)
#             file.write(folder)
#             file.truncate()
#     def get_RGB(self):
#         folder=tk.filedialog.askopenfilename(initialdir="/",title='Input RGB File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.RGB.set(folder)
#         self.button8.configure(state='normal')        

#     def get_NIR(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input NIR File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.NIR.set(folder)

#     def get_DEM(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input DEM File',filetypes=(("TIF","*.tiff"),("all files","*.*")))
#         self.DEM.set(folder)

#     def get_Thermal(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input Thermal File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.Thermal.set(folder)

#     def get_outfile(self):
#         folder=tk.filedialog.asksaveasfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title = 'Output TIF file',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         folder += '.tiff'
#         self.out_file.set(folder)

#     def run(self):
#         if self.out_file.get() == 'blank':
#             tk.messagebox.showinfo("Select Outfile", "Please define an outfile")
#         else:    
#             self._toggle_state('disabled')
#             layerstacker(self.shapefiles.get(),self.groundDEM.get(),self.RGB.get(),self.NIR.get(),self.DEM.get(),self.Thermal.get(),self.out_file.get())
#             tk.messagebox.showinfo("Processing Complete", "Processing Complete")
#             self._toggle_state('normal')

#     def _toggle_state(self, state):
#         state = state if state in ('normal', 'disabled') else 'normal'
#         widgets = (self.button1, self.button2,self.button3,self.button4,self.button5,self.button6,self.button7,self.button8,self.button10)
#         for widget in widgets:
#             widget.configure(state=state)


#     def __init__(self,parent,controller):
#         tk.Frame.__init__(self,parent)
#         self.grid_rowconfigure(12, weight=1)
#         self.grid_columnconfigure(0, weight=1)
#         self.grid_columnconfigure(4, weight=1)

#          #---VARIABLES---#
#         self.RGB=tk.StringVar()
#         self.RGB.set('blank')
#         self.out_file=tk.StringVar()
#         self.out_file.set('blank')
#         self.NIR=tk.StringVar()
#         self.NIR.set('blank')
#         self.DEM=tk.StringVar()
#         self.DEM.set('blank')
#         self.Thermal=tk.StringVar()
#         self.Thermal.set('blank')
#         self.shapefiles=tk.StringVar()
#         self.shapefiles.set(fieldshape)
#         self.groundDEM=tk.StringVar()
#         self.groundDEM.set(grnddem)

#         self.label=tk.Label(self,text='Data Cube Generator',font=Large_Font)
#         self.label.columnconfigure(0,weight=1)
#         self.label.grid(row=0,column=2,padx=10)

#         self.button1=ttk.Button(self,text='Back to Home Page',command=lambda: controller.show_frame(HomePage))
#         self.button1.columnconfigure(0,weight=1)
#         self.button1.grid(row=9,column=2,pady=10)

#         self.button2=ttk.Button(self,text='RGB',command=self.get_RGB,width=18)
#         self.button2.columnconfigure(0,weight=1)
#         self.button2.grid(row=3,column=1,pady=10)
#         self.button3=ttk.Button(self,text='NIR',command=self.get_NIR,width=18)
#         self.button3.columnconfigure(0,weight=1)
#         self.button3.grid(row=4,column=1,pady=10)
#         self.button4=ttk.Button(self,text='DEM',command=self.get_DEM,width=18)
#         self.button4.columnconfigure(0,weight=1)
#         self.button4.grid(row=5,column=1,pady=10)
#         self.button5=ttk.Button(self,text='Thermal',command=self.get_Thermal,width=18)
#         self.button5.columnconfigure(0,weight=1)
#         self.button5.grid(row=6,column=1,pady=10)
#         self.button6=ttk.Button(self,text='Shapefile',command=self.get_shapefile,width=18)
#         self.button6.columnconfigure(0,weight=1)
#         self.button6.grid(row=1,column=1,pady=10)
#         self.button7=ttk.Button(self,text='Out File',command=self.get_outfile,width=18)
#         self.button7.columnconfigure(0,weight=1)
#         self.button7.grid(row=7,column=1,pady=10)
#         self.button8=ttk.Button(self,text='Run',command=self.run,width=18)
#         self.button8.columnconfigure(0,weight=1)
#         self.button8.configure(state='disabled')
#         self.button8.grid(row=8,column=2,pady=10)
#         self.button9=ttk.Button(self,text='Bare Ground DEM',command=self.get_grndDEM,width=18)
#         self.button9.columnconfigure(0,weight=1)
#         self.button9.grid(row=2,column=1,pady=10)

#         self.button10=ttk.Button(self,text='Quit',command=controller.enditall)
#         self.button10.columnconfigure(0,weight=1)
#         self.button10.grid(row=10,column=2)

#          #---ENTRIES---#
#         self.entry1=ttk.Entry(self,textvariable=self.shapefiles,width=75)
#         self.entry1.columnconfigure(0,weight=1)
#         self.entry1.grid(row=1,column=2,padx=5)
#         self.entry1=ttk.Entry(self,textvariable=self.groundDEM,width=75)
#         self.entry1.columnconfigure(0,weight=1)
#         self.entry1.grid(row=2,column=2,padx=5)
#         self.entry2=ttk.Entry(self,textvariable=self.RGB,width=75)
#         self.entry2.columnconfigure(0,weight=1)
#         self.entry2.grid(row=3,column=2,padx=5)
#         self.entry3=ttk.Entry(self,textvariable=self.NIR,width=75)
#         self.entry3.columnconfigure(0,weight=1)
#         self.entry3.grid(row=4,column=2,padx=5,pady=5)
#         self.entry4=ttk.Entry(self,textvariable=self.DEM,width=75)
#         self.entry4.columnconfigure(0,weight=1)
#         self.entry4.grid(row=5,column=2,padx=5,pady=5)
#         self.entry5=ttk.Entry(self,textvariable=self.Thermal,width=75)
#         self.entry5.columnconfigure(0,weight=1)
#         self.entry5.grid(row=6,column=2,padx=5,pady=5)
#         self.entry6=ttk.Entry(self,textvariable=self.out_file,width=75)
#         self.entry6.columnconfigure(0,weight=1)
#         self.entry6.grid(row=7,column=2,padx=5,pady=5)

# ## Data Extractor Class: defines the variables for the appearance and function of the Data Extraction Tool.
# class DataExtractor(ttk.Frame):
#     def get_shapefile(self):
#         global shapefolder
#         global file
#         folder=tk.filedialog.askdirectory(initialdir = "/",title = 'Shapefiles')
#         if folder==shapefolder:
#             pass
#         else:
#             self.shapefiles.set(folder+'/')
#             file.seek(0)
#             file.write(folder+'/')
#             file.truncate()
#     def get_grndDEM(self):
#         global groundfile
#         global grnddem
#         folder=tk.filedialog.askopenfilename(initialdir = "/",title = 'Bare Ground DEM',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         if folder==grnddem:
#             pass
#         else:
#             self.groundDEM.set(folder)
#             with open(resource_path('groundDEM.txt'),'r+') as f2:
#                 f2.seek(0)
#                 f2.write(folder)
#             file.truncate()
        
#     def get_RGB(self):
#         folder=tk.filedialog.askopenfilename(initialdir="/",title='Input RGB File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.RGB.set(folder)
#         self.button8.configure(state='normal')        

#     def get_NIR(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input NIR File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.NIR.set(folder)
#         self.button8.configure(state='normal')       

#     def get_DEM(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input DEM File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.DEM.set(folder)
#         self.button8.configure(state='normal')        

#     def get_Thermal(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input Thermal File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.Thermal.set(folder)
#         self.button8.configure(state='normal')        
        
#     def get_datacube(self):
#         folder=tk.filedialog.askopenfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"../")),title='Input Datacube File',filetypes=(("TIF","*.tif"),("all files","*.*")))
#         self.datacube.set(folder)
#         self.button8.configure(state='normal')

#     def get_outfile(self):
#         folder=tk.filedialog.asksaveasfilename(initialdir=os.path.abspath(os.path.join(self.RGB.get() ,"..a/")),title = 'Output excel file',filetypes=(("xlsx","*.xlsx"),("all files","*.*")))
#         print(folder)
#         if '.xlsx' in folder:
#             self.out_file.set(folder)
#         else:
#             folder += '.xlsx'
#             self.out_file.set(folder)

#     def run(self):
#         if self.out_file.get() == 'blank':
#             tk.messagebox.showinfo("Select Outfile", "Please define an outfile")
#         else:    
#             self._toggle_state('disabled')
#             if not self.groundDEM.get():
#                 self.groundDEM.set('blank')
#             layers={'RGB':self.RGB.get(),'NIR':self.NIR.get(),'Thermal':self.Thermal.get(),'groundDEM':self.groundDEM.get(),'datacube':self.datacube.get(),'DEM':self.DEM.get()}
#             variables={'outfile':self.out_file.get(),'shapefiles':self.shapefiles.get()}
#             try:
#                 data_extractor(variables,layers)
#                 tk.messagebox.showinfo("Processing Complete", "Processing Complete")
#                 self._toggle_state('normal')
#             except Exception as e:
#                 tk.messagebox.showerror("Error", e)
#                 traceback.print_exc()
#                 self._toggle_state('normal')

#     def _toggle_state(self, state):
#         state = state if state in ('normal', 'disabled') else 'normal'
#         widgets = (self.button1, self.button2,self.button3,self.button4,self.button5,self.button6,self.button7,self.button8,self.button10,self.button9)
#         for widget in widgets:
#             widget.configure(state=state)
#     def __init__(self,parent,controller):
#         tk.Frame.__init__(self,parent)
#         self.grid_rowconfigure(12, weight=1)
#         self.grid_columnconfigure(0, weight=1)
#         self.grid_columnconfigure(4, weight=1)

#          #---VARIABLES---#
#         self.RGB=tk.StringVar()
#         self.RGB.set('blank')
#         self.out_file=tk.StringVar()
#         self.out_file.set('blank')
#         self.NIR=tk.StringVar()
#         self.NIR.set('blank')
#         self.DEM=tk.StringVar()
#         self.DEM.set('blank')
#         self.Thermal=tk.StringVar()
#         self.Thermal.set('blank')
#         self.shapefiles=tk.StringVar()
#         self.shapefiles.set(shapefolder)
#         self.groundDEM=tk.StringVar()
#         self.groundDEM.set(grnddem)
#         self.datacube=tk.StringVar()
#         self.datacube.set('blank')

#         self.label=tk.Label(self,text='Data Extraction',font=Large_Font)
#         self.label.columnconfigure(0,weight=1)
#         self.label.grid(row=0,column=2,padx=10)

#         self.button6=ttk.Button(self,text='Shapefile',command=self.get_shapefile,width=18)
#         self.button6.columnconfigure(0,weight=1)
#         self.button6.grid(row=1,column=1,pady=10)
#         self.button9=ttk.Button(self,text='Bare Ground DEM',command=self.get_grndDEM,width=18)
#         self.button9.columnconfigure(0,weight=1)
#         self.button9.grid(row=2,column=1,pady=10)        
#         self.button2=ttk.Button(self,text='RGB',command=self.get_RGB,width=18)
#         self.button2.columnconfigure(0,weight=1)
#         self.button2.grid(row=3,column=1,pady=10)
#         self.button3=ttk.Button(self,text='NIR',command=self.get_NIR,width=18)
#         self.button3.columnconfigure(0,weight=1)
#         self.button3.grid(row=4,column=1,pady=10)
#         self.button4=ttk.Button(self,text='DEM',command=self.get_DEM,width=18)
#         self.button4.columnconfigure(0,weight=1)
#         self.button4.grid(row=5,column=1,pady=10)
#         self.button5=ttk.Button(self,text='Thermal',command=self.get_Thermal,width=18)
#         self.button5.columnconfigure(0,weight=1)
#         self.button5.grid(row=6,column=1,pady=10)
#         self.button10=ttk.Button(self,text='DataCube',command=self.get_datacube,width=18)
#         self.button10.columnconfigure(0,weight=1)
#         self.button10.grid(row=7,column=1,pady=10)
#         self.button7=ttk.Button(self,text='Out File',command=self.get_outfile,width=18)
#         self.button7.columnconfigure(0,weight=1)
#         self.button7.grid(row=9,column=1,pady=10)
#         self.button8=ttk.Button(self,text='Run',command=self.run,width=18)
#         self.button8.columnconfigure(0,weight=1)
#         self.button8.configure(state='disabled')
#         self.button8.grid(row=10,column=2,pady=10)


# #        self.button11=ttk.Button(self,text='Bare Ground DEM',command=self.get_grndDEM,width=18)
# #        self.button11.columnconfigure(0,weight=1)
# #        self.button11.grid(row=2,column=1,pady=10)

#         self.button1=ttk.Button(self,text='Back to Home Page',command=lambda: controller.show_frame(HomePage))
#         self.button1.columnconfigure(0,weight=1)
#         self.button1.grid(row=11,column=2,pady=10)
#         self.button10=ttk.Button(self,text='Quit',command=controller.enditall)
#         self.button10.columnconfigure(0,weight=1)
#         self.button10.grid(row=12,column=2)

#          #---ENTRIES---#
#         self.entry1=ttk.Entry(self,textvariable=self.shapefiles,width=75)
#         self.entry1.columnconfigure(0,weight=1)
#         self.entry1.grid(row=1,column=2,padx=5)
#         self.entry2=ttk.Entry(self,textvariable=self.groundDEM,width=75)
#         self.entry2.columnconfigure(0,weight=1)
#         self.entry2.grid(row=2,column=2,padx=5)
#         self.entry3=ttk.Entry(self,textvariable=self.RGB,width=75)
#         self.entry3.columnconfigure(0,weight=1)
#         self.entry3.grid(row=3,column=2,padx=5)
#         self.entry4=ttk.Entry(self,textvariable=self.NIR,width=75)
#         self.entry4.columnconfigure(0,weight=1)
#         self.entry4.grid(row=4,column=2,padx=5,pady=5)
#         self.entry5=ttk.Entry(self,textvariable=self.DEM,width=75)
#         self.entry5.columnconfigure(0,weight=1)
#         self.entry5.grid(row=5,column=2,padx=5,pady=5)
#         self.entry6=ttk.Entry(self,textvariable=self.Thermal,width=75)
#         self.entry6.columnconfigure(0,weight=1)
#         self.entry6.grid(row=6,column=2,padx=5,pady=5)
#         self.entry7=ttk.Entry(self,textvariable=self.datacube,width=75)
#         self.entry7.columnconfigure(0,weight=1)
#         self.entry7.grid(row=7,column=2,padx=5,pady=5)
#         self.entry8=ttk.Entry(self,textvariable=self.out_file,width=75)
#         self.entry8.columnconfigure(0,weight=1)
#         self.entry8.grid(row=9,column=2,padx=5,pady=5)

## Shapefile Class: defines the variables for the appearance and function of the shapefile generator Tool.
class Shapefilegenerator(ttk.Frame):
    def get_shapefile(self):
        folder=tk.filedialog.askopenfilename(initialdir = "/",title = 'Shapefile',filetypes=(("shp","*.shp"),("all files","*.*")))
        self.shapefile.set(folder)
        
    def get_outfolder(self):
        folder=tk.filedialog.askdirectory(initialdir = "/",title = 'Outfolder')
        print(folder)
        self.out_folder.set(folder+'/')
        self._toggle_state('normal')
        
    def run(self):
        if self.out_folder.get() == 'blank':
            tk.messagebox.showinfo("Select Outfolder", "Please define an outfolder")
        else:    
            self._toggle_state('disabled')
            try:
                shapefile_gen(self.shapefile.get(),self.out_folder.get())
                tk.messagebox.showinfo("Processing Complete", "Processing Complete")
                self._toggle_state('normal')
            except Exception as e:
                tk.messagebox.showerror("Error", e)
                traceback.print_exc()
                self._toggle_state('normal')

    def _toggle_state(self, state):
        state = state if state in ('normal', 'disabled') else 'normal'
        widgets = (self.button1, self.button2,self.button3)
        for widget in widgets:
            widget.configure(state=state)
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        self.grid_rowconfigure(12, weight=1)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(4, weight=1)

         #---VARIABLES---#
        self.shapefile=tk.StringVar()
        self.shapefile.set('blank')
        self.out_folder=tk.StringVar()
        self.out_folder.set('blank')

        self.label=tk.Label(self,text='Shapefile Generator',font=Large_Font)
        self.label.columnconfigure(0,weight=1)
        self.label.grid(row=0,column=2,padx=10)

        self.button1=ttk.Button(self,text='Shapefile',command=self.get_shapefile,width=18)
        self.button1.columnconfigure(0,weight=1)
        self.button1.grid(row=1,column=1,pady=10)
        self.button2=ttk.Button(self,text='Outfolder',command=self.get_outfolder,width=18)
        self.button2.columnconfigure(0,weight=1)
        self.button2.grid(row=2,column=1,pady=10)
        self.button3=ttk.Button(self,text='Run',command=self.run,width=18)
        self.button3.columnconfigure(0,weight=1)
        self.button3.configure(state='disabled')
        self.button3.grid(row=10,column=2,pady=10)


# #        self.button11=ttk.Button(self,text='Bare Ground DEM',command=self.get_grndDEM,width=18)
# #        self.button11.columnconfigure(0,weight=1)
# #        self.button11.grid(row=2,column=1,pady=10)

        self.button11=ttk.Button(self,text='Back to Home Page',command=lambda: controller.show_frame(HomePage))
        self.button11.columnconfigure(0,weight=1)
        self.button11.grid(row=11,column=2,pady=10)
        self.button10=ttk.Button(self,text='Quit',command=controller.enditall)
        self.button10.columnconfigure(0,weight=1)
        self.button10.grid(row=12,column=2)

          #---ENTRIES---#
        self.entry1=ttk.Entry(self,textvariable=self.shapefile,width=75)
        self.entry1.columnconfigure(0,weight=1)
        self.entry1.grid(row=1,column=2,padx=5)
        self.entry2=ttk.Entry(self,textvariable=self.out_folder,width=75)
        self.entry2.columnconfigure(0,weight=1)
        self.entry2.grid(row=2,column=2,padx=5)


app=software()
app.geometry("1280x720")
app.mainloop()